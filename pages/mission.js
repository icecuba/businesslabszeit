import React from 'react';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import Layout from '../components/MyLayout.js';

import {UnderLink, UnderLinkWhite, QuoteText, BenefitGrid} from '../components/UiComponents';

const AnimatedGrid = styled(Grid)`
  background-image: url('https://media.giphy.com/media/O7ooKGOlJNKFO/giphy.gif');
`;

const HeroHeaderContainer = styled.div``;

const MissionPage = () => (
  <Layout>
    <HeroHeaderContainer>
      <AnimatedGrid>
        <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.5}}> </Grid.Unit>
        <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.5}}>
          {' '}
          <QuoteText>
            BusinessLabs was one of the first SaaS focused accelerators in Europe. Founded in Dornbirn, Austria in 2018.
            We are helping to build the next generation of great SaaS companies.
          </QuoteText>
        </Grid.Unit>
      </AnimatedGrid>
      <Grid>
        <BenefitGrid size={{mobile: 1, tablet: 0.25, desktop: 0.25}} />
        <BenefitGrid size={{mobile: 1, tablet: 0.5, desktop: 0.5}}>
          {' '}
          <UnderLink>Programm Offering</UnderLink> With our twelve month program and hands-on mentorship we act as an
          extension of our teams and help them prosper. We invite you to join us – together, we will build the future of
          technology. BusinessLabs is a start-up platform founded in Dornbirn, Austria. We focus on the intersection of
          Software as Service and Open Source.
        </BenefitGrid>
        <BenefitGrid size={{mobile: 1, tablet: 0.25, desktop: 0.25}} />
      </Grid>
    </HeroHeaderContainer>
  </Layout>
);

export default MissionPage;
