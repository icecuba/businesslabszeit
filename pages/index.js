import Layout from '../components/MyLayout.js';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import {UnderLink, LargeH3} from '../components/UiComponents';
import HeroHeader from '../components/HeroHeader';
import BlueQuote from '../components/BlueQuote';
import PhotoGrid from '../components/PhotoGrid';

import HomepageStaticBlog from '../components/HomepageStaticBlog';
import BlueLogo from '../components/BlueLogo';

const BlueWhiteLogo = styled.div``;

export default () => (
  <Layout>
    <HeroHeader />
    <BlueQuote />
    <PhotoGrid />

    <HomepageStaticBlog />
  </Layout>
);

/* <BlueLogo /> */
