import React from 'react';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import Layout from '../components/MyLayout.js';

import {UnderLink, UnderLinkWhite, QuoteText} from '../components/UiComponents';

const AnimatedGrid = styled(Grid)`
  background-image: url('https://media.giphy.com/media/hU4sqErKAitVe/giphy.gif');
`;

const HeroHeaderContainer = styled.div``;

const ContactPage = () => (
  <Layout>
    <HeroHeaderContainer>
      <AnimatedGrid>
        <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.5}}> </Grid.Unit>
        <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.5}}>
          {' '}
          <QuoteText>
            <p>
              <UnderLink>BusinessLabs</UnderLink>
            </p>
            <p>Färbergasse 17b</p>
            <p>6850 Dornbirn</p>

            <a href="mailto:hello@businesslabs.io">
              <UnderLink>Hello@BusinessLabs.io</UnderLink>
            </a>

            <p> </p>

            <p>
              <UnderLink>Contact</UnderLink>
            </p>
            <p>Guntram Bechtold</p>
            <p>+43 664 35 66 618 </p>
          </QuoteText>
        </Grid.Unit>
      </AnimatedGrid>
    </HeroHeaderContainer>
  </Layout>
);

export default ContactPage;
