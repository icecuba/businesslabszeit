import React from 'react';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import Layout from '../components/MyLayout.js';

import {UnderLink, UnderLinkWhite, QuoteText} from '../components/UiComponents';

const AnimatedGrid = styled(Grid)`
  background-image: url('https://media.giphy.com/media/cvtgBBebUEFTG/giphy.gif');
`;

const HeroHeaderContainer = styled.div``;

const ApplyPage = () => (
  <Layout>
    <HeroHeaderContainer>
      <AnimatedGrid>
        <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.5}}> </Grid.Unit>
        <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.5}}>
          {' '}
          <QuoteText>
            We're building the next generation of <UnderLink>Saas</UnderLink> and <UnderLink>OpenSource</UnderLink>{' '}
            businesses by working with a other founders, using a unique body of knowledge and networking with industry
            experts from around the globe.{' '}
            <UnderLink>
              <a href="mailto:hello@businesslabs.io">Apply for BusinessLabs</a>
            </UnderLink>
          </QuoteText>
        </Grid.Unit>
      </AnimatedGrid>
    </HeroHeaderContainer>
  </Layout>
);

export default ApplyPage;
