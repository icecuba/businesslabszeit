import React from 'react';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import Layout from '../components/MyLayout.js';

import {UnderLink, UnderLinkWhite, QuoteText, BenefitGrid} from '../components/UiComponents';

const AnimatedGrid = styled(Grid)`
  background-image: url('https://media.giphy.com/media/9rLP24t0EQJWw/giphy.gif');
`;

const HeroHeaderContainer = styled.div``;

const ProgramPage = () => (
  <Layout>
    <HeroHeaderContainer>
      <AnimatedGrid>
        <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.5}}> </Grid.Unit>
        <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.5}}>
          {' '}
          <QuoteText>
            Business Labs is for dedicated Entrepeneurs: The Vorarlberg region offers a wealth of business expertise in
            engineering, electronics, finance, data and analytics, retail among many others.
          </QuoteText>
        </Grid.Unit>
      </AnimatedGrid>
    </HeroHeaderContainer>

    <Grid>
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}} />
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}}>
        <UnderLink>Programm Offering</UnderLink> BusinessLabs leverages the community in which we live in to accelerate
        your business. Our startups provide solutions to the industry sweet spots of our region in order to maximize
        customer and business development opportunities.
      </BenefitGrid>
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}} />
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}}>
        <UnderLink>12 MONTH PROGRAM</UnderLink> We’re one of the longest programs in the nation at 12 months. Our 10+2
        approach means you’ll still get the support you need following our Demo Day.
      </BenefitGrid>
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}}>
        <UnderLink>MENTORSHIP</UnderLink> We find the right people at the right time to give you exactly what you need.
        Our wide network of mentors are experts in their fields with extensive connections.
      </BenefitGrid>
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}}>
        <UnderLink>LEGAL AND ACCOUNTING</UnderLink> You come with a team and we help complete it. Our program partners
        are awesome by providing discounted legal and accounting services to each of our participants.
      </BenefitGrid>
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}}>
        <UnderLink>STARTUP EDUCATION</UnderLink> We joke that we provide a mini-MBA, but it’s not far from the truth. We
        offer a top notch startup curriculum blending today’s best business model development methods, customer
        acquisition strategies, project management principles and product development practices.
      </BenefitGrid>
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}}>
        <UnderLink>AGILE PROCESS</UnderLink> We facilitate the sharing of challenges, knowhow and vision by having a
        weekly planning workshop. Topics include technology, design and entrepreneurial. Trough this process we see peer
        mentors effectively becoming a part of your team. Through one-on-one, weekly meetings he helps you hurdle your
        most challenging road blocks.
      </BenefitGrid>
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}}>
        <UnderLink>CENTRAL LOCATION</UnderLink> The Vorarlberg area offers many regional benefits to startups. Quality
        of living, small town feel, easy access to business partners, centralize european location, many large cities
        like munich, zürich and milano in reach.
      </BenefitGrid>
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}} />><BenefitGrid
        size={{mobile: 1, tablet: 1, desktop: 0.3}}
      >
        <UnderLink>ADDITIONAL PROGRAM</UnderLink> PERKS 12
        <ol>
          <li>Months Free Office Space and Meeting Rooms.</li>
          <li>Gigabit Internet</li>
          <li>Drinks and Snacks are on us.</li>
          <li>We have an actual garage. </li>
          <li>Secured Locker for Storage Apartment</li>
          <li>A game Great Community</li>
          <li>Restaurants on the street </li>
          <li>Inner City with bars in walking distance</li>
          <li>Vorarlberg Car-Sharing in the same building</li>
          <li>Workshops with the community</li>
          <li>Regular social events</li>
        </ol>
      </BenefitGrid>
      <BenefitGrid size={{mobile: 1, tablet: 1, desktop: 0.3}} />>
    </Grid>
  </Layout>
);

export default ProgramPage;
