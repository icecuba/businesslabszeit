import Head from 'next/head';
import PropTypes from 'prop-types';
import React from 'react';

/* import {getImagePath} from '../util/mediaHelpers'; */

export default class Header extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    image: PropTypes.string,
    keywords: PropTypes.string,
    canonical: PropTypes.string,
    robots: PropTypes.string,
    children: PropTypes.element
  };

  static defaultProps = {
    title: 'BusinessLabs.io - SAAS and OS Incubator',
    description: `BusinessLabs is where the next generation of Saas and OpenSource businesses build innovative products that are only possible by employing a unique body of knowledge, an inspiring mindset and industry experts from around the globe. `,
    robots: 'follow'
  };

  _getOgImage = image => {
    if (!image) {
      return 'https://www.businesslabs.io/static/images/og.jpg';
    }
    return getImagePath(image.filePath, 800, 800);
  };
  render() {
    return (
      <Head>
        <title>{this.props.title}</title>
        <meta charSet="UTF-8" />
        <meta name="mobile-web-app-capable" content="yes" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="googlebot" content="index,follow,noodp" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
        <meta name="title" content={this.props.title} />
        <meta name="description" content={this.props.description} />
        <meta name="keywords" content={this.props.keywords} />
        <meta name="robots" content={this.props.robots} />

        <meta property="language" content="de_AT" />
        <meta property="publisher" content="BusinessLabs.io" />
        <meta property="author" content="BusinessLabs.io" />
        <meta property="copyright" content="BusinessLabs.io" />
        <meta property="audience" content="all" />
        <meta property="distribution" content="global" />
        <meta property="image" content={this._getOgImage(this.props.image)} />
        <meta property="format-detection" content="telephone=yes" />

        <meta property="og:title" content={this.props.title} />
        <meta property="og:description" content={this.props.description} />
        <meta property="og:locale" content="de_AT" />
        <meta property="og:image" content={this._getOgImage(this.props.image)} />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="BusinessLabs.io" />

        <meta property="DC.Title" content={this.props.title} />
        <meta property="DC.Publisher" content="BusinessLabs.io" />
        <meta property="DC.Copyright" content="BusinessLabs.io" />

        <meta property="twitter:card" content="summary" />
        <meta property="twitter:title" content={this.props.title} />
        <meta property="twitter:description" content={this.props.description} />
        <meta property="twitter:image" content={this._getOgImage(this.props.image)} />

        <link rel="apple-touch-icon" sizes="180x180" href="/static/favicons/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/static/favicons/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/static/favicons/favicon-16x16.png" />
        <link rel="manifest" href="/static/favicons/site.webmanifest" />
        <link rel="mask-icon" href="/static/favicons/safari-pinned-tab.svg" color="#39484d" />
        <link rel="shortcut icon" href="/static/favicons/favicon.ico" />
        <meta name="msapplication-TileColor" content="#2b5797" />
        <meta name="msapplication-config" content="/static/favicons/browserconfig.xml" />
        <meta name="theme-color" content="#ffffff" />
        {this.props.children}
      </Head>
    );
  }
}
