import React from 'react';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import {UnderLinkWhite} from './UiComponents';

const BlogGrid = styled(Grid)`
  background: #294d9b;
  line-height: 4em;
  color: #fff;
  padding: 20px;
  overflow: hidden;
`;

const BlueQuoteHeadline = styled.div`
  font-family: 'Ropa Sans';
  font-size: 60px;
`;
const BlueQuoteContent = styled.div``;

const BlueQuote = styled.div`
  background: #294d9b;
  padding: 30vh 30vw 30vh 30vw;
  line-height: 4em;
  color: #fff;
`;

const BlueQuoteBox = () => (
  <BlogGrid>
    <Grid.Unit size={{mobile: 1, tablet: 0.5, desktop: 0.5}}>
      <BlueQuoteHeadline>Welcome to BusinessLabs</BlueQuoteHeadline>
      <BlueQuoteContent>
        BusinessLabs is a start-up platform founded in Dornbirn, Europe. We focus on the intersection of{' '}
        <UnderLinkWhite>Software as Service</UnderLinkWhite> and <UnderLinkWhite>Open Source</UnderLinkWhite>. We build
        products, run thematic workshops, support early stage entrepreneurs and provide physical space for founders.
      </BlueQuoteContent>
    </Grid.Unit>
  </BlogGrid>
);

export default BlueQuoteBox;
