import React from 'react';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import {UnderLink} from './UiComponents';
import GridUnit from 'styled-components-grid/dist/cjs/components/GridUnit';

const BlogGrid = styled(Grid)`
  padding: 20px;
`;

const BlogGridUnit = styled(Grid.Unit)`
  overflow: hidden;
`;
const BlogHeadline = styled.div`
  font-family: 'Ropa Sans';
  font-size: 60px;
`;

const BlogPost = styled.div`
  margin: 80px 0 0 0;
`;

const BlogPostHeadline = styled.div`
  margin: 30vh 0 0 0;
  font-family: 'Ropa Sans';
  font-size: 40px;
`;

const BlogPostHeadlineDate = styled.div`
  margin: 3vh 0 0 0;
  font-size: 20px;
`;
const BlogPostHeadlineAuthor = styled.div`
  font-size: 20px;
  color: #294d9b;
`;
const BlogContent = styled.div`
  line-height: 4em;
  color: #000;
`;

const BlogBox = () => (
  <BlogGrid horizontalAlign="center">
    <BlogGridUnit>
      <BlogHeadline>Updates and Announcements</BlogHeadline>
      <BlogPost>
        <BlogPostHeadline>This is new</BlogPostHeadline>
        <BlogPostHeadlineDate>Guntram Bechtold, 10.June 2018</BlogPostHeadlineDate>

        <BlogContent>
          We're exited to announce the launch of Business Labs. At BusinessLabs we are dedicated to build {'  '}
          <UnderLink> Software as Service</UnderLink> and <UnderLink>Open Source</UnderLink> with the goal of making
          everyones life better by delivering new value and services to the people. We build products, run thematic
          workshops, support early stage entrepreneurs and provide physical space for founders.
        </BlogContent>
      </BlogPost>
      <BlogPost>
        <BlogPostHeadline>Weekly Progress Update</BlogPostHeadline>
        <BlogPostHeadlineDate>Guntram Bechtold, 10.May 2018</BlogPostHeadlineDate>

        <BlogContent>
          With the launch of BusinessLabs we're doing a weekly planning Meeting get updated in the status quo and
          elaborate about the next steps. We've decided to strategyize, plan and proceed using the agile framework
          #scrum. If you want to learn more about how people work we encourage you to get in touch with us by emailing
          to <UnderLink href="mailto:Hello@BusinessLabs.io">hello@BusinessLabs.io</UnderLink> and get the discussion
          going.
        </BlogContent>
      </BlogPost>
    </BlogGridUnit>
  </BlogGrid>
);

export default BlogBox;
