import React from 'react';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import {UnderLink, UnderLinkWhite, QuoteText} from './UiComponents';

const AnimatedGrid = styled(Grid)`
  background-image: url('https://media.giphy.com/media/svgKS032qi2yI/giphy.gif');
`;

const HeroHeaderContainer = styled.div``;

const HeroHeader = () => (
  <HeroHeaderContainer>
    <AnimatedGrid>
      <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.5}}> </Grid.Unit>
      <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.5}}>
        {' '}
        <QuoteText>
          We're building the next generation of <UnderLink>Saas</UnderLink> and <UnderLink>OpenSource</UnderLink>{' '}
          businesses by working with a other founders, using a unique body of knowledge and networking with industry
          experts from around the globe. Apply for BusinessLabs
        </QuoteText>
      </Grid.Unit>
    </AnimatedGrid>
  </HeroHeaderContainer>
);

export default HeroHeader;
