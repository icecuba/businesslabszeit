import Link from 'next/link';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import {UnderLink} from '../components/UiComponents';

const NavigationGrid = styled(Grid)`
  margin: 40px;
`;
const LogoUnit = styled(Grid.Unit)`
  background-color: #fff;
`;

const LinkUnit = styled(Grid.Unit)`
  background-color: #fff;
  margin: 20px;
`;

const NavLink = styled.a`
  text-decoration: none;
  border-bottom: 4px solid #294d9b;
  box-shadow: inset 0 -4px 0 #294d9b;
  color: #4a4a4a;
  text-transform: uppercase;
  transition: background 0.1s cubic-bezier(0.33, 0.66, 0.66, 1);
  cursor: pointer;
  &:hover {
    color: #fff;
    background: #294d9b;
  }
`;

const Header = () => (
  <NavigationGrid valign="center">
    <LogoUnit size={{mobile: 1, tablet: 1, desktop: 0.2}}>
      <a href="/">
        <img src="static/images/BusinessLabsLogo.svg" />
      </a>
    </LogoUnit>
    <Grid.Unit size={{mobile: 1, tablet: 1, desktop: 0.25}} />
    <LinkUnit size={{mobile: 1, tablet: 1, desktop: 0.1}}>
      <Link href="/mission">
        <UnderLink>Mission</UnderLink>
      </Link>
    </LinkUnit>
    <LinkUnit size={{mobile: 1, tablet: 1, desktop: 0.1}}>
      <Link href="/program">
        <UnderLink>Program</UnderLink>
      </Link>
    </LinkUnit>
    <LinkUnit size={{mobile: 1, tablet: 1, desktop: 0.1}}>
      <Link href="/apply">
        <UnderLink>Apply</UnderLink>
      </Link>
    </LinkUnit>
    <LinkUnit size={{mobile: 1, tablet: 1, desktop: 0.1}}>
      <Link href="/contact">
        <UnderLink>Contact</UnderLink>
      </Link>
    </LinkUnit>
  </NavigationGrid>
);

export default Header;
