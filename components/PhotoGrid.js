import React from 'react';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import {UnderLinkWhite} from './UiComponents';
import MdPersonAdd from 'react-icons/lib/md/person-add';

const PhotoGrid = styled(Grid)``;
const Photo = styled(Grid.Unit)``;
const ImageBox = styled.div`
  position: relative;
  width: 100%;
  height: auto;
  background-color: #294d9b;
`;

const Image = styled.img`
  width: 100%;
  height: auto;
  transition: transform 0.5s; /* Animation */
  &:hover {
    transform: scale(1.3);
    box-shadow: 0 15px 55px rgba(44, 77, 155, 0.3);
    border-radius: 10px;
  }
`;

const CompanyBox = styled.div`
  position: absolute;
  bottom: 5vh;
  left: 5vh;
`;
const CompanyHeadline = styled.div`
  font-family: 'Ropa Sans';
  font-size: 30px;
  color: #fff;
`;

const PhotoGridBox = () => (
  <PhotoGrid>
    <Photo size={{mobile: 1, tablet: 0.5, desktop: 0.25}}>
      <ImageBox>
        <a href="http://www.quizium.com/">
          <Image src="../static/images/Team_Andreas_Lukas.jpg" />
        </a>
        <CompanyBox>
          <a href="http://www.quizium.com/">
            <CompanyHeadline>Quizium.com</CompanyHeadline>
          </a>
          <a href="https://twitter.com/aFINKndreas">
            <MdPersonAdd color="#efefef" size={40} />
          </a>
        </CompanyBox>
      </ImageBox>
    </Photo>
    <Photo size={{mobile: 1, tablet: 0.5, desktop: 0.25}}>
      <ImageBox>
        <a href="http://www.Multivative.com/">
          <Image src="../static/images/Team_Multivative.jpg" />
        </a>
        <CompanyBox>
          <a href="http://www.Multivative.com/">
            <CompanyHeadline>Multivative.com</CompanyHeadline>
          </a>
          <a href="https://www.facebook.com/multivative">
            <MdPersonAdd color="#efefef" size={40} />
          </a>
        </CompanyBox>
      </ImageBox>
    </Photo>
    <Photo size={{mobile: 1, tablet: 0.5, desktop: 0.25}}>
      <ImageBox>
        <a href="http://www.Cluevo.at/">
          <Image src="../static/images/Team_Cluevo.jpg" />
        </a>
        <CompanyBox>
          <a href="http://www.Cluevo.at/">
            <CompanyHeadline>Cluevo.at</CompanyHeadline>
          </a>
          <a href="https://www.instagram.com/enversonbay/">
            <MdPersonAdd color="#efefef" size={40} />
          </a>
        </CompanyBox>
      </ImageBox>
    </Photo>
    <Photo size={{mobile: 1, tablet: 0.5, desktop: 0.25}}>
      <ImageBox>
        <Image src="../static/images/Team_BusinessLabs.jpg" />
        <CompanyBox>
          <a href="http://www.BusinessLabs.io/">
            <CompanyHeadline>BusinessLabs.io</CompanyHeadline>
          </a>
          <a href="https://www.facebook.com/BusinessLabs.io/">
            <MdPersonAdd color="#efefef" size={40} />
          </a>
        </CompanyBox>
      </ImageBox>
    </Photo>
  </PhotoGrid>
);

export default PhotoGridBox;
