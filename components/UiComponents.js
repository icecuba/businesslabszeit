import styled from 'styled-components';
import Grid from 'styled-components-grid';

export const UnderLink = styled.a`
  text-decoration: none;
  border-bottom: 4px solid #294d9b;
  box-shadow: inset 0 -4px 0 #294d9b;
  color: #4a4a4a;
  text-transform: uppercase;
  transition: background 0.1s cubic-bezier(0.33, 0.66, 0.66, 1);
  cursor: pointer;
  &:hover {
    color: #fff;
    background: #294d9b;
  }
`;

export const UnderLinkWhite = styled(UnderLink)`
  border-bottom: 4px solid #fff;
  box-shadow: inset 0 -4px 0 #fff;
  color: #fff;
  &:hover {
    color: #000;
    background: #fff;
  }
`;

export const QuoteText = styled.div`
  padding: 20vh 15vw 20vh 15vw;
  line-height: 4em;
  background: #fff;
  opacity: 0.95;
`;

export const LargeH3 = styled.h3``;

export const BenefitGrid = styled(Grid.Unit)`
  padding: 40px;
  line-height: 30px;
`;
