import React from 'react';
import styled from 'styled-components';

const DarkContainer = styled.div`
  background: #000;
  padding: 3vh 3wh 3vh 3wh;
  line-height: 4em;
  color: #fff;
`;

const Footer = ({footerInfo}) => (
  <DarkContainer>
    {' '}
    {console.log(footerInfo)}
    BusinessLabs copyright 2018
  </DarkContainer>
);

export default Footer;
