import Header from './Header';
import Footer from '../components/Footer';
import {injectGlobal} from 'styled-components';
import Head from './Head';

const pageInfos = {
  ogImage: '',
  seoDescription:
    'BusinessLabs is where the next generation of Saas and OpenSource businesses build innovative products that are only possible by employing a unique body of knowledge, an inspiring mindset and industry experts from around the globe.',
  seoTitle: 'BusinessLabs.io - Next generation of Saas and OpenSource Business'
};

const footerInfo = {
  year: '2018'
};

injectGlobal`
  /* open-sans-300 - latin */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: url('../static/fonts/open-sans-v15-latin-300.eot'); /* IE9 Compat Modes */
  src: local('Open Sans Light'), local('OpenSans-Light'),
       url('../static/fonts/open-sans-v15-latin-300.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('../static/fonts/open-sans-v15-latin-300.woff2') format('woff2'), /* Super Modern Browsers */
       url('../static/fonts/open-sans-v15-latin-300.woff') format('woff'), /* Modern Browsers */
       url('../static/fonts/open-sans-v15-latin-300.ttf') format('truetype'), /* Safari, Android, iOS */
       url('../static/fonts/open-sans-v15-latin-300.svg#OpenSans') format('svg'); /* Legacy iOS */
}
/* open-sans-regular - latin */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: url('../static/fonts/open-sans-v15-latin-regular.eot'); /* IE9 Compat Modes */
  src: local('Open Sans Regular'), local('OpenSans-Regular'),
       url('../static/fonts/open-sans-v15-latin-regular.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('../static/fonts/open-sans-v15-latin-regular.woff2') format('woff2'), /* Super Modern Browsers */
       url('../static/fonts/open-sans-v15-latin-regular.woff') format('woff'), /* Modern Browsers */
       url('../static/fonts/open-sans-v15-latin-regular.ttf') format('truetype'), /* Safari, Android, iOS */
       url('../static/fonts/open-sans-v15-latin-regular.svg#OpenSans') format('svg'); /* Legacy iOS */
}
/* open-sans-600 - latin */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: url('../static/fonts/open-sans-v15-latin-600.eot'); /* IE9 Compat Modes */
  src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'),
       url('../static/fonts/open-sans-v15-latin-600.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('../static/fonts/open-sans-v15-latin-600.woff2') format('woff2'), /* Super Modern Browsers */
       url('../static/fonts/open-sans-v15-latin-600.woff') format('woff'), /* Modern Browsers */
       url('../static/fonts/open-sans-v15-latin-600.ttf') format('truetype'), /* Safari, Android, iOS */
       url('../static/fonts/open-sans-v15-latin-600.svg#OpenSans') format('svg'); /* Legacy iOS */
}
/* ropa-sans-regular - latin */
@font-face {
  font-family: 'Ropa Sans';
  font-style: normal;
  font-weight: 400;
  src: url('../static/fonts/ropa-sans-v7-latin-regular.eot'); /* IE9 Compat Modes */
  src: local('Ropa Sans Regular'), local('RopaSans-Regular'),
       url('../static/fonts/ropa-sans-v7-latin-regular.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('../static/fonts/ropa-sans-v7-latin-regular.woff2') format('woff2'), /* Super Modern Browsers */
       url('../static/fonts/ropa-sans-v7-latin-regular.woff') format('woff'), /* Modern Browsers */
       url('../static/fonts/ropa-sans-v7-latin-regular.ttf') format('truetype'), /* Safari, Android, iOS */
       url('../static/fonts/ropa-sans-v7-latin-regular.svg#RopaSans') format('svg'); /* Legacy iOS */
}
  }

  body {
    margin: 0;
    font-family: "Open Sans", Arial, Helvetica, sans-serif;
  }
`;

const Layout = props => (
  <div>
    <Head image={pageInfos.ogImage} description={pageInfos.seoDescription} title={pageInfos.seoTitle} />
    <Header />
    {props.children}
    <Footer year={footerInfo} />
  </div>
);

export default Layout;
