import React from 'react';
import styled from 'styled-components';
import {UnderLinkWhite} from './UiComponents';

const WhiteBlueCircle = styled.div``;

const BlueContainer = styled.div`
  background: #294d9b;
  padding: 30vh 30vw 30vh 30vw;
  line-height: 4em;
  color: #fff;
`;

const BlueLogo = () => (
  <BlueContainer>
    <WhiteBlueCircle>
      <img src="static/images/BusinessLabsLogoWhite.svg" />
      <div />
    </WhiteBlueCircle>
  </BlueContainer>
);

export default BlueLogo;
